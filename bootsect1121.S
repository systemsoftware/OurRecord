 #====================================================================== 书51====================================================================================================
 
 #====================================================================== 开始====================================================================================================
 #====================================================================== 开始====================================================================================================

#include <asm/boot.h>

SETUPSECTS	= 4			/* default nr of setup-sectors */
BOOTSEG		= 0x07C0		/* original address of boot-sector */# 起始地址
INITSEG		= DEF_INITSEG		/* we move boot here - out of the way */# 移动到的地址
SETUPSEG	= DEF_SETUPSEG		/* setup starts here */
SYSSEG		= DEF_SYSSEG		/* system loaded at 0x10000 (65536) */
SYSSIZE		= DEF_SYSSIZE		/* system size: # of 16-byte clicks */
					/* to be loaded */
ROOT_DEV	= 0 			/* ROOT_DEV is now written by "build" */
SWAP_DEV	= 0			/* SWAP_DEV is now written by "build" */

#ifndef SVGA_MODE
#define SVGA_MODE ASK_VGA
#endif

#ifndef RAMDISK
#define RAMDISK 0
#endif

#ifndef ROOT_RDONLY
#define ROOT_RDONLY 1
#endif

.code16
.text

.global _start
_start:

# First things first. Move ourself from 0x7C00 -> 0x90000 and jump there.
 #====================================================================== 移动到9c00====================================================================================================

	# 加载起始位置7c00
	movw	$BOOTSEG, %ax                 
	movw	%ax, %ds		# %ds = BOOTSEG
	movw	$INITSEG, %ax
	movw	%ax, %es		# %ax = %es = INITSEG
	movw	$256, %cx
	subw	%si, %si
	subw	%di, %di
	cld                 # 清除方向标志位
	rep					# 移动到9c00
	movsw
	ljmp	$INITSEG, $go
#===========================================================================================================================================================================================
# bde - changed 0xff00 to 0x4000 to use debugger at 0x6400 up (bde).  We
# wouldn't have to worry about this if we checked the top of memory.  Also
# my BIOS can be configured to put the wini drive tables in high memory
# instead of in the vector table.  The old stack might have clobbered the
# drive table.

 #====================================================================== 设置栈 9000:3ff4====================================================================================================
go:	movw	$0x4000-12, %di		# 0x4000 is an arbitrary value >=
					# length of bootsect + length of
					# setup + room for stack;
					# 12 is disk parm size.
	movw	%ax, %ds		# %ax and %es already contain INITSEG
	movw	%ax, %ss
	movw	%di, %sp		# put stack at INITSEG:0x4000-12.

#===========================================================================================================================================================================================
# Many BIOS's default disk parameter tables will not recognize
# multi-sector reads beyond the maximum sector number specified
# in the default diskette parameter tables - this may mean 7
# sectors in some cases.
#
# Since single sector reads are slow and out of the question,
# we must take care of this by creating new parameter tables
# (for the first disk) in RAM.  We will set the maximum sector
# count to 36 - the most we will encounter on an ED 2.88.  
#
# High doesn't hurt.  Low does.
#
# Segments are as follows: %cs = %ds = %es = %ss = INITSEG, %fs = 0,
# and %gs is unused.

 #====================================================================== 拷贝软盘参数表====================================================================================================
	movw	%cx, %fs		# %fs = 0
	movw	$0x78, %bx		# %fs:%bx is parameter table address 0000：:0078
	pushw	%ds				
	ldsw	%fs:(%bx), %si		# %ds:%si is source	#存储的是一个地址#这条指令格式是LDS reg,mem这条指令的功能是把mem指向的地址,高位存放在DS中,低位存放在reg中.
	movb	$6, %cl			# copy 12 bytes
	pushw	%di			# %di = 0x4000-12.
	
	rep				# don't worry about cld
	movsw				# already done above
	popw	%di
	popw	%ds
	
	movb	$36, 0x4(%di)		# patch sector count#修改扇区数36
	movw	%di, %fs:(%bx)

	movw	%es, %fs:2(%bx)

#===========================================================================================================================================================================================
# Get disk drive parameters, specifically number of sectors/track.

# It seems that there is no BIOS call to get the number of sectors.
# Guess 36 sectors if sector 36 can be read, 18 sectors if sector 18
# can be read, 15 if sector 15 can be read.  Otherwise guess 9.
# Note that %cx = 0 from rep movsw above.
#====================================================================== 读setup.s 获取扇区数====================================================================================================

	movw	$disksizes, %si		# table of sizes to try              si=18，36
	
probe_loop:
	lodsb               #LODS是指令规定源操作数为(DS:SI)，目的操作数隐含为AL（字节）或AX（字）寄存器。从9000：1224？？？？读数到al？？？？
	cbtw				# extend to word
	movw	%ax, sectors
	cmpw	$disksizes+4, %si
	jae	got_sectors		# If all else fails, try 9

	xchgw	%cx, %ax		# %cx = track and sector
	xorw	%dx, %dx		# drive 0, head 0 清零0号软盘 0号磁头
	movw	$0x0200, %bx		# address = 512, in INITSEG (%es = %cs) 缓冲区地址 9000：0200
	movw	$0x0201, %ax		# service 2, 1 sector 02功能号 01读一个扇区
	int	$0x13
	jc	probe_loop		# try next value

	#(3)、功能02H 
	# 功能描述：读扇区 
	# 入口参数：AH＝02H 
	# AL＝扇区数 
	# CH＝柱面 
	# CL＝扇区 
	# DH＝磁头 
	# DL＝驱动器，00H~7FH：软盘；80H~0FFH：硬盘 
	# ES:BX＝缓冲区的地址 
	# 出口参数：CF＝0——操作成功，AH＝00H，AL＝传输的扇区数，否则，AH＝状态代码，参见功能号01H中的说明   
    #原文：https://blog.csdn.net/ylo523/article/details/39050771 
	
#===========================================================================================================================================================================================




	
	
	

#====================================================================== 更改光标 输出loading====================================================================================================

	
got_sectors:
	movb	$0x03, %ah		# read cursor pos
	xorb	%bh, %bh
	int	$0x10
	
	# 4、功能03H
# 功能描述：在文本坐标下，读取光标各种信息
# 入口参 数：AH＝03H
# BH＝显示页码
# 出口参数：CH＝光标的起始行
# CL＝光标的终止行
# DH＝行(Y坐标)
# DL＝列 (X坐标)




	movw	$9, %cx
	movb	$0x07, %bl		# page 0, attribute 7 (normal)
					# %bh is set above; int10 doesn't
					# modify it
	movw	$msg1, %bp
	movw	$0x1301, %ax		# write string, move cursor
	int	$0x10			# tell the user we're loading.. 输出loading
#===========================================================================================================================================================================================

# Load the setup-sectors directly after the moved bootblock (at 0x90200).
# We should know the drive geometry to do it, as setup may exceed first
# cylinder (for 9-sector 360K and 720K floppies).

	movw	$0x0001, %ax		# set sread (sector-to-read) to 1 as
	movw	$sread, %si		# the boot sector has already been read                         sread=0
	movw	%ax, (%si)                                                           #sread=1;

	xorw	%ax, %ax		# reset FDC  磁盘复位
	xorb	%dl, %dl
	int	$0x13
	
	
	movw	$0x0200, %bx		# address = 512, in INITSEG

	next_step:
	movb	setup_sects, %al       #al=4    # ax=0004 bx=0200 cx= dx= 0
	movw	sectors, %cx               # ax=0004 bx=0200 cx=40 dx= 0
	subw	(%si), %cx		# (%si) = sread  # ax=xx04 bx=0200 cx=sectors-1 dx= 0
	cmpb	%cl, %al        # ax=xx04 bx=0200 cx=sectors-1 dx= 0
	jbe	no_cyl_crossing
	movw	sectors, %ax
	subw	(%si), %ax		# (%si) = sread
no_cyl_crossing:
	call	read_track    # ax=0004 bx=0200 cx=sectors-1 dx= 0   读4个扇区到90200处
	pushw	%ax			# save it
	call	set_next		# set %bx properly; it uses %ax,%cx,%dx     # ax=0005 bx=1000 cx=0800 dx= 0 sread=5；
	popw	%ax			# restore					# ax=0004 bx=1000 cx=0800 dx= 0 sread=5；
	subb	%al, setup_sects	# rest - for next step		# ax=0004 bx=1000 cx=0800 dx= 0 sread=5；
	jnz	next_step

	pushw	$SYSSEG			#0x1000
	popw	%es			# %es = SYSSEG  #0x1000  # ax=0004 bx=1000 cx=0800 dx= 0  es=1000 sread=5
	call	read_it
	call	kill_motor
	call	print_nl

#===========================================================================================================================================================================================
# After that we check which root-device to use. If the device is
# defined (!= 0), nothing is done and the given device is used.
# Otherwise, one of /dev/fd0H2880 (2,32) or /dev/PS0 (2,28) or /dev/at0 (2,8)
# depending on the number of sectors we pretend to know we have.

# Segments are as follows: %cs = %ds = %ss = INITSEG,
#	%es = SYSSEG, %fs = 0, %gs is unused.
#==============================================================记录根设备类型=============================================================================================================================

	movw	root_dev, %ax
	orw	%ax, %ax
	jne	root_defined

	movw	sectors, %bx
	movw	$0x0208, %ax		# /dev/ps0 - 1.2Mb
	cmpw	$15, %bx
	je	root_defined

	movb	$0x1c, %al		# /dev/PS0 - 1.44Mb
	cmpw	$18, %bx
	je	root_defined

	movb	$0x20, %al		# /dev/fd0H2880 - 2.88Mb
	cmpw	$36, %bx
	je	root_defined

	movb	$0, %al			# /dev/fd0 - autodetect
root_defined:
	movw	%ax, root_dev

# After that (everything loaded), we jump to the setup-routine
# loaded directly after the bootblock:
#===========================================================================================================================================================================================


#==============================================================蹦到9000：0200=============================================================================================================================

	ljmp	$SETUPSEG, $0

#===========================================================================================================================================================================================


#========================================================================结束===================================================================================================================
#========================================================================结束===================================================================================================================


















# These variables are addressed via %si register as it gives shorter code.

sread:	.word 0				# sectors read of current track   当前磁道扇区数目
head:	.word 0				# current head
track:	.word 0				# current track

# This routine loads the system at address SYSSEG, making sure
# no 64kB boundaries are crossed. We try to load it as fast as
# possible, loading whole tracks whenever we can.


#==================================================================读核心=========================================================================================================================

read_it:   # ax=0004 bx=1000 cx=0800 dx= 0  es=1000 sread=5
	movw	%es, %ax		# %es = SYSSEG when called		 # ax=1000 bx=1000 cx=0800 dx= 0  es=1000 sread=5
	testw	$0x0fff, %ax

die:	jne	die			# %es must be at 64kB boundary
	
	
	
	xorw	%bx, %bx		# %bx is starting address within segment    # ax=1000 bx=0000 cx=0800 dx= 0  es=1000 sread=5

rp_read:
#ifdef __BIG_KERNEL__			# look in setup.S for bootsect_kludge      此处有一个分支 一个是大内核 一个是小内核

	bootsect_kludge = 0x220		# 0x200 + 0x20 which is the size of the
	lcall	bootsect_kludge		# bootsector + bootsect_kludge offset



#else				 # ax=1000 bx=0000 cx=0800 dx= 0  es=1000 sread=5
	movw	%es, %ax	 # ax=1000 bx=0000 cx=0800 dx= 0  es=1000 sread=5
	subw	$SYSSEG, %ax  # ax=0000 bx=0000 cx=0800 dx= 0  es=1000 sread=5
	movw	%bx, %cx	# ax=0000 bx=0000 cx=0000 dx= 0  es=1000 sread=5
	shr	$4, %cx     	# ax=0000 bx=0000 cx=0000 dx= 0  es=1000 sread=5
	add	%cx, %ax		# check offset  # ax=0000 bx=0000 cx=0000 dx= 0  es=1000 sread=5
#endif
	cmpw	syssize, %ax		# have we loaded everything yet?
	jbe	ok1_read

	ret
#===========================================================================================================================================================================================

ok1_read:						# ax=0000 bx=0000 cx=0000 dx= 0  es=1000 sread=5
	movw	sectors, %ax		# ax=sectors bx=0000 cx=0000 dx= 0  es=1000 sread=5
	subw	(%si), %ax		# (%si) = sread   # ax=sectors-5 bx=0000 cx=0000 dx= 0  es=1000 sread=5
	movw	%ax, %cx        # ax=sectors-5 bx=0000 cx=sectors-5 dx= 0  es=1000 sread=5
	shlw	$9, %cx			# ax=sectors-5 bx=0000 cx=sectors-5+512 dx= 0  es=1000 sread=5
	addw	%bx, %cx		# ax=sectors-5 bx=0000 cx=sectors-5+512 dx= 0  es=1000 sread=5
	jnc	ok2_read

	je	ok2_read

	xorw	%ax, %ax
	subw	%bx, %ax
	shrw	$9, %ax
ok2_read:
	call	read_track
	call	set_next
	jmp	rp_read

read_track:
	pusha     # ax=xx04 bx=0200 cx=sectors-1 dx= 0 
	pusha	
	movw	$0xe2e, %ax 		# loading... message 2e = . 显示字符=      # ax=0e2e bx=0200 cx=sectors-1 dx= 0
	
	movw	$7, %bx		 # ax=0e2e bx=0007  cx=sectors-1 dx= 0
 	int	$0x10
	popa	 # ax=xx04 bx=0200 cx=sectors-1 dx= 0 
	
# 14、功能0EH
# 功能描述：在Teletype模式下显示字符
# 入口参数：AH＝0EH
# AL＝ 字符
# BH＝页码
# BL＝前景色(图形模式)
# 出口参数：无




# Accessing head, track, sread via %si gives shorter code.

	movw	4(%si), %dx		# 4(%si) = track      # ax=xx04 bx=0200 cx=sectors-1 dx= track
	movw	(%si), %cx		# (%si)  = sread      # ax=xx04 bx=0200 cx=1 dx= track
	incw	%cx    									# ax=xx04 bx=0200 cx=2 dx= track
	movb	%dl, %ch							# ax=xx04 bx=0200 cx=(track)02 dx= track
	movw	2(%si), %dx		# 2(%si) = head     # ax=xx04 bx=0200 cx=(track)02 dx= head
	movb	%dl, %dh							 # ax=xx04 bx=0200 cx=(track)02 dx= (head)00
	andw	$0x0100, %dx	#只能是1或者0	 # ax=xx04 bx=0200 cx=(track)02 dx= (head)00
	movb	$2, %ah							 # ax=0204 bx=0200 cx=(track)02 dx= (head)00
	pushw	%dx			# save for error dump
	pushw	%cx
	pushw	%bx
	pushw	%ax								 # ax=0204 bx=0200 cx=(track)02 dx= (head)00    es=9000
	int	$0x13
	jc	bad_rt

	addw	$8, %sp
	popa									# ax=xx04 bx=0200 cx=sectors-1 dx= 0 
	ret

set_next:# ax=0004 bx=0200 cx=sectors-1 dx= 0
	movw	%ax, %cx		# ax=0004 bx=0200 cx=0004 dx= 0
	addw	(%si), %ax		# (%si) = sread # ax=0005 bx=0200 cx=0004 dx= 0
	cmp	sectors, %ax		# ax=0005 bx=0200 cx=0004 dx= 0
	jne	ok3_set
	movw	$0x0001, %ax
	xorw	%ax, 2(%si)		# change head
	jne	ok4_set
	incw	4(%si)			# next track
ok4_set:
	xorw	%ax, %ax
ok3_set:					# ax=0005 bx=0200 cx=0004 dx= 0
	movw	%ax, (%si)		# set sread		# ax=0005 bx=0200 cx=0004 dx= 0 sread=5；
	shlw	$9, %cx							# ax=0005 bx=0200 cx=0800 dx= 0 sread=5；
	addw	%cx, %bx						# ax=0005 bx=1000 cx=0800 dx= 0 sread=5；
	jnc	set_next_fin
	movw	%es, %ax
	addb	$0x10, %ah
	movw	%ax, %es
	xorw	%bx, %bx
set_next_fin:
	ret

bad_rt:
	pushw	%ax			# save error code
	call	print_all		# %ah = error, %al = read
	xorb	%ah, %ah
	xorb	%dl, %dl
	int	$0x13
	addw	$10, %sp
	popa
	jmp read_track

# print_all is for debugging purposes.  
#
# it will print out all of the registers.  The assumption is that this is
# called from a routine, with a stack frame like
#
#	%dx 
#	%cx
#	%bx
#	%ax
#	(error)
#	ret <- %sp
 
print_all:
	movw	$5, %cx			# error code + 4 registers
	movw	%sp, %bp
print_loop:
	pushw	%cx			# save count remaining
	call	print_nl		# <-- for readability
	cmpb	$5, %cl
	jae	no_reg			# see if register name is needed
	
	movw	$0xe05 + 'A' - 1, %ax
	subb	%cl, %al
	int	$0x10
	movb	$'X', %al
	int	$0x10
	movb	$':', %al
	int	$0x10
no_reg:
	addw	$2, %bp			# next register
	call	print_hex		# print it
	popw	%cx
	loop	print_loop
	ret

print_nl:
	movw	$0xe0d, %ax		# CR
	int	$0x10
	movb	$0xa, %al		# LF
	int 	$0x10
	ret

# print_hex is for debugging purposes, and prints the word
# pointed to by %ss:%bp in hexadecimal.

print_hex:
	movw	$4, %cx			# 4 hex digits
	movw	(%bp), %dx		# load word into %dx
print_digit:
	rolw	$4, %dx			# rotate to use low 4 bits
	movw	$0xe0f, %ax		# %ah = request
	andb	%dl, %al		# %al = mask for nybble
	addb	$0x90, %al		# convert %al to ascii hex
	daa				# in only four instructions!
	adc	$0x40, %al
	daa
	int	$0x10
	loop	print_digit
	ret

# This procedure turns off the floppy drive motor, so
# that we enter the kernel in a known state, and
# don't have to worry about it later.
# NOTE: Doesn't save %ax or %dx; do it yourself if you need to.

kill_motor:
#if 1
	xorw	%ax, %ax		# reset FDC
	xorb	%dl, %dl
	int	$0x13
#else
	movw	$0x3f2, %dx
	xorb	%al, %al
	outb	%al, %dx
#endif
	ret

sectors:	.word 0
disksizes:	.byte 36, 18, 15, 9
msg1:		.byte 13, 10
		.ascii "Loading"

# XXX: This is a fairly snug fit.

.org 497
setup_sects:	.byte SETUPSECTS
root_flags:	.word ROOT_RDONLY
syssize:	.word SYSSIZE
swap_dev:	.word SWAP_DEV
ram_size:	.word RAMDISK
vid_mode:	.word SVGA_MODE
root_dev:	.word ROOT_DEV
boot_flag:	.word 0xAA55
